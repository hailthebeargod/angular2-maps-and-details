import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AgmCoreModule } from '../../node_modules/angular2-google-maps/core';

import { AppComponent } from './app.component';
import { LocationsComponent } from './locations.component';
import { LocationDetailsComponent } from './location-details.component';

import { SpotService } from './spot.service';
import { ContentService } from './content.service';

const appRoutes: Routes = [
  { path: 'details/:id', component: LocationDetailsComponent },
  { path: '', component: LocationsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LocationsComponent,
    LocationDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAAjp_EttQNrYdHA5kXTmqtF6Za33YVeAY'
    })
  ],
  providers: [
    SpotService,
    ContentService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
