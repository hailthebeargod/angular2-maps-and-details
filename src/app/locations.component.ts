import { Component, OnInit } from '@angular/core';

import { GeoLocation } from './geo-location';
import { Spot } from './spot';

import { SpotService } from './spot.service';

import {
  SebmGoogleMap,
  SebmGoogleMapMarker,
  SebmGoogleMapInfoWindow
} from 'angular2-google-maps/core';

@Component({
  selector: 'locations',
  templateUrl: './locations.component.html',
  styles: [ '.sebm-google-map-container { height: 450px; }' ]
})
export class LocationsComponent{

  locations: Spot[];

  // coordinates to center map on
  // looks like 59.87903 longitude is incorrect, since API returns fixed values far from it
  center: GeoLocation = {
    lat: 59.879037,
    lng: 8.575609
  };

  constructor(private locationService: SpotService) {}

  // retrieves nearby locations (for central coordinate) for google map
  ngOnInit(): void {
    this.locationService.getLocations(this.center).then(locations => this.locations = locations);
  }  

}