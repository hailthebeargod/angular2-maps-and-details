// class to describe geographical location
export class GeoLocation {
    lat: number;
    lng: number;
}