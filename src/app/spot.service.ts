import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { Spot } from './spot';
import { GeoLocation } from './geo-location';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class SpotService {
    
    constructor(private http: Http) {};

    private apiUrl = 'http://api.dev.aispot.no/lighthouse/spot/nearby';
    
    // retrieves location, by location (coordinates currently) provided
    getLocations(geo: GeoLocation): Promise<Spot[]> {
        return this.http.get(this.apiUrl + `?lat=${geo.lat}&lng=${geo.lng}`)
            .toPromise()
            .then(response => response.json() as Spot[])
            .catch(this.reportError);
    }
    
    // handle error occured during API request
    reportError(error: any): Promise<any> {
        alert('An error occured');
        return Promise.reject(error.message || error);
    }

}