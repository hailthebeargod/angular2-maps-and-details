// describes button configurations returned via content API
export class Button {
    type: string;
    text: string;
    color: string;
    actionData: string;
}