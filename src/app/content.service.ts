import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { Content } from './content';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ContentService {
    
    constructor(private http: Http) {};

    private apiUrl = 'http://api.dev.aispot.no/lighthouse/content/';
    
    // retrieves spot details, by id provided
    getDetails(id: number): Promise<Content> {
        return this.http.get(this.apiUrl + id)
            .toPromise()
            .then(response => response.json() as Content)
            .catch(this.reportError);
    }

    // handle error occured during API request
    reportError(error: any): Promise<any> {
        alert('An error occured');
        return Promise.reject(error.message || error);
    }

}