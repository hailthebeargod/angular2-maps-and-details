import { Button } from './button';

// describes spot information, returned by respective API
export class Content {
    id: number;
    type: string;
    headerTitle: string;
    title: string;
    description: string;
    imageUrl: string;
    buttons: Button[];
}