import { GeoLocation } from './geo-location';

export class Spot {
    id: number;
    title: string;
    description: string;
    location: GeoLocation;
}