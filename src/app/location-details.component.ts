import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Content } from './content';

import { ContentService } from './content.service';

@Component({
  selector: 'location-details',
  templateUrl: './location-details.component.html',
  styles: ['img { width: 100% }']
})

export class LocationDetailsComponent implements OnInit {

  info: Content;

  constructor(private detailService: ContentService, private route: ActivatedRoute) {}

  // retrieves spot details from content API, by spot ID provided
  ngOnInit(): void {
    this.detailService.getDetails(+this.route.snapshot.params['id'])
      .then(details => this.info = details);
  }

}
